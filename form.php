<!DOCTYPE html>
<html lang="en">

<head>
  <title>kkkkk34</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="util.css">
  <link rel="stylesheet" type="text/css" href="main.css">
  <link rel="stylesheet" type="text/css" href="css/style1.css" />
  <style>
  /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
  .error {
  border: 2px solid red;
}
    </style>
  </head>
</head>

<body style="background-color: #666666;">

  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <form class="login100-form validate-form" action="" method="post">
          <?php
            if (!empty($messages)) {
              print('<div id="messages">');
              // Выводим все сообщения.
              foreach ($messages as $message) {
                print($message);
              }
              print('</div>');
            }

            // Далее выводим форму отмечая элементы с ошибками классом error
            // и задавая начальные значения элементов ранее сохраненными.
          ?>
          
          <div class="wrap-input100 validate-input">
            <input class="input100 <?php if ($errors['fio']||$errors['wfio']) {print 'error';} ?>" type="text" name="fio" placeholder="ФИО" 
               value="<?php print $values['fio']; ?>">
            <span class="focus-input100"></span>
            <span class="label-input100"></span>
          </div>

          <div class="wrap-input100 validate-input">
            <input class="input100 <?php if ($errors['email']) {print 'error';} ?>" type="text" name="email" placeholder="email"
            value="<?php print $values['email']; ?>">
            <span class="focus-input100"></span>
            <span class="label-input100"></span>
          </div>

          <div class="wrap-input100 validate-input">
            <input class="input100 <?php if ($errors['date']) {print 'error';} ?>" type="text" name="date" placeholder="ГГГГ-ММ-ДД"
            value="<?php print $values['date']; ?>">
            <span class="focus-input100"></span>
            <span class="label-input100"></span>
          </div>

          <div class="wrapper <?php if ($errors['sex']) {print 'error';} ?>">
            <h4>Пол:</h4> <br>
            <input class="radio" id="radio-1" type="radio" name="sex" value="male" <?php if ($values['sex'] == 'male') print('checked') ?>/>
            <label for="radio-1">мужской</label>
            <br /><br />
            <input class="radio" id="radio-2" type="radio" name="sex" value="female" <?php if ($values['sex'] == 'female') print('checked') ?>/>
            <label for="radio-2">женский</label>
          </div>
          <br>

         
          <h4> Количество конечностей:</h4> <br>
          <div <?php if ($errors['amountOfLimbs']) {print 'class="error"';}?>>
            <label>
              <input type="radio" name="amountOfLimbs" value="1" <?php if ($values['amountOfLimbs'] == '1') print('checked') ?>>1</label>
            <label>
              <input type="radio" name="amountOfLimbs" value="2" <?php if ($values['amountOfLimbs'] == '2') print('checked') ?>>2</label>
            <label>
              <input type="radio" name="amountOfLimbs" value="3" <?php if ($values['amountOfLimbs'] == '3') print('checked') ?>>3</label>
            <label>
              <input type="radio" name="amountOfLimbs" value="6" <?php if ($values['amountOfLimbs'] == '6') print('checked') ?>>6</label>
            <label>
              <input type="radio" name="amountOfLimbs" value="8" <?php if ($values['amountOfLimbs'] == '8') print('checked') ?>>8</label>
            <label>
              <input type="radio" name="amountOfLimbs" value="10" <?php if ($values['amountOfLimbs'] == '10') print('checked') ?>>10</label>
          </div>
          <br>
          <div <?php if ($errors['abilities']) {print 'class="error"';}?>>
            <select size="4" multiple name = "abilities[]">
              <option disabled >Суперспособности:</option>
              <option value="immortality" <?php if (stripos($values['abilities'], "ortality")) print('selected');  ?>>Бессмертие</option>
              <option value="fly" <?php if (stripos($values['abilities'], "fly")) print('selected');  ?>>Полёт</option>
              <option value="shapelessness" <?php if (stripos($values['abilities'], "lessness")) print('selected');  ?>>Бесформенность</option>
            </select> 
          </div>
          <br>
            <textarea name = "biography" placeholder="about..." rows="3"<?php if ($errors['biography']||$errors['wbiography']){print 'class="error"';} ?>><?php print($values['biography']); ?></textarea>
          <div class="flex-sb-m w-full p-t-3 p-b-32">
            <div class="contact100-form-checkbox">
              <input class="input-checkbox100" id="ckb1" type="checkbox" name="ch" <?php if ($values['ch'] == 'on') print('checked');?>>
              <label class="label-checkbox100 <?php if ($errors['ch']) {print 'error';} ?>"  for="ckb1">
                С политикой формы согласен
              </label>
            </div>

          </div>

          <div class="container-login100-form-btn">
            <button class="login100-form-btn">
              Send
            </button>
          </div>
        </form>
        <?php print($values['abilities']); ?>
        <div class="login100-more" style="background-image: url('bg-01.jpg');">
        </div>
      </div>
    </div>
  </div>

</body>

</html>